## Notes for formatting purposes

```

\```
Code block
\```

**Bold**

__AlsoBold__

*Italics*

~~Strikethrough~~

**_BoldItalics_**

> Quote

[URL Text](https://help.github.com/en/articles/basic-writing-and-formatting-syntax#quoting-code)

- List
- Elements
- With Bullets

1. List
2. Elements
3. With Numbers
   - Indented with bullets (align with previous line's first character (after #._)
     - Double Indented Bullet
     
- [x] Task 1 (checked box)
- [ ] Task 2 (unchecked box)
- [ ] Task 3

```

## Results in:

```
Code block
```

**Bold**

__AlsoBold__

*Italics*

~~Strikethrough~~

**_BoldItalics_**

> Quote

[URL Text](https://help.github.com/en/articles/basic-writing-and-formatting-syntax#quoting-code)

- List
- Elements
- With Bullets

1. List
2. Elements
3. With Numbers
   - Indented with bullets (align with previous line's first character (after #._)
     - Double Indented Bullet
     
- [x] Task 1 (checked box)
- [ ] Task 2 (unchecked box)
- [ ] Task 3 (unchecked box)