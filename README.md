# Readme

## Project
Create a ASCII RPG game in console using C++.

## Contributors
@PhilipNeff

@DavinSalo

## How to launch
The provided code does not include a working game from the onset. The game needs to be compiled on your machine.

In order to compile the game open your console of preference in the same directory as README.md and type the following:

```

To Be Added, scons script or Makefile?

```

To play the game:

```

To Be Added, run the executable created or add a subscript "make play" or "make run"

```